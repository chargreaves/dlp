﻿using System;
using System.Collections.Generic;
using System.Text;

using LV_Client;
using CommInterface;

//using CoreToolkit;
//using Instrument;
//using Logging;
//using Equipment;

namespace DLP_IO8Implementation
{
    //public class createEquipmentObject
    //{
    //    private object equipmentObject;

    //    public createEquipmentObject()
    //    {
    //        equipmentObject = new KeysightU2000Impl();
    //    }
    //    public object getEquipmentObject()
    //    {
    //        return equipmentObject;
    //    }
    //};

    public class DLP_IO8Impl
    {
        private LVClient m_CoreToolkit;
        private ICommInterface dlpSession;
        private int m_iUutInstance;

        public DLP_IO8Impl()
        {
            m_CoreToolkit = new LVClient();
            dlpSession = new ICommInterface();
            dlpSession = (ICommInterface)m_CoreToolkit.getEquipmentObject("DLP_COMM", m_iUutInstance.ToString());

        }

        public void EnableLED()
        {
            dlpSession.write("3");
            dlpSession.write("R");
        }

        public void DisableLED()
        {
            dlpSession.write("E");
            dlpSession.write("4");
        }

        public void SetTemperatureF()
        {
            dlpSession.write("L");
        }

        public void SetTemperatureC()
        {
            dlpSession.write(";");
        }

        public string Ping()
        {
            string strResponse = "";

            dlpSession.write("'");

            dlpSession.read(ref strResponse, 1);

            return strResponse;
        }

        public string Ping1()
        {
            string strResponse = "";

            dlpSession.write("'");

            dlpSession.read(ref strResponse, 1);

            return strResponse;
        }

        public string ReadTemperature()
        {
            string strResponse = "";

            dlpSession.clear();

            dlpSession.write("]");

            dlpSession.read(ref strResponse, 1024);

            return strResponse;

        }

        //private ILogging log = null;

        //public override int preActivation(equipmentConfig equip)
        //{
        //    equipConfig = equip;
        //    equipmentName = equip.logicalName;

        //    log = LogControl.GetLogger(ILogging.LogSource.SerialPort, System.Reflection.MethodBase.GetCurrentMethod().DeclaringType + " " + equipmentName);
        //    log.Debug("preActivation() complete.");

        //    return 0;
        //}

        //public override int initialize(string strDeviceAddress, string strBusId, string strTimeout, string sDeviceName)
        //{            
        //    try
        //    {
        //        int status = base.initialize(strDeviceAddress, strBusId, strTimeout, sDeviceName);

        //        if (status == 0)
        //        {
        //            status = reset();
        //            status = directWrite("*IDN?");
        //            string identifier = "";
        //            if (0 == status)
        //                status = directRead(out identifier, 255);
        //        }

        //        log.Debug("initialize() complete.");

        //        return status;
        //    }
        //    catch (Exception exc)
        //    {
        //        log.Error("initialize " + equipmentName + " threw an exception:\r\n", exc);
        //        return -1;
        //    }
        //}

        //public override int postActivation(Equipment.equipmentConfig equip)
        //{
        //    double dReading = FetchChannel(0);
        //    log.DebugFormat("Fetch reading={0}", dReading);

        //    log.Debug("postActivation() complete.");

        //    return 0;
        //}

        //private bool directWriteAndRead(string cmd, out string response)
        //{
        //    response = string.Empty;

        //    int status = directWrite(cmd);

        //    if (0 == status)
        //    {
        //        status = directRead(out response, 255);
        //        return true;
        //    }

        //    return false;
        //}


        //// Implementation here

        //public override void Zero() { }

        //public override void Calibrate(int Channel) { }

        //public override double FetchChannel(int Channel)
        //{
        //    double lfMeas = -999.9;

        //    try
        //    {
        //        string reading;

        //        bool bStatus= directWriteAndRead("FETCH?", out reading);

        //        if (bStatus == true)
        //            Double.TryParse(reading, out lfMeas);
        //    }
        //    catch (Exception exc)
        //    {
        //        log.Error("FetchChannel() threw an exception:\r\n", exc);
        //    }

        //    return lfMeas;
        //}

        //public double FetchChannelchris(int Channel)
        //{
        //    double lfMeas = -999.9;

        //    try
        //    {
        //        string reading;

        //        bool bStatus = directWriteAndRead("FETCH?", out reading);

        //        if (bStatus == true)
        //            Double.TryParse(reading, out lfMeas);
        //    }
        //    catch (Exception exc)
        //    {
        //        log.Error("FetchChannelchris() threw an exception:\r\n", exc);
        //    }

        //    return lfMeas;
        //}

    }

}
